"""
+-+
| |
+-+-+
  | |
  +-+-+
    | |
    +-+-+
      | |
      +-+
"""

num_blocks = int(input())
ends = '+-+-+'
middle = '| |'
space = '  '

# first block is special
print("+-+",
      "| |", sep='\n')

for i in range(num_blocks-1):
    print(space * i + ends)
    print(space * (i+1) + middle)

# last block is special
print(space * (num_blocks-1) + '+-+')
