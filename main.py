def repeatChar(numRepeats, outputChar):
    space = 0
    for colNo in range(1, numRepeats + 1):
        print(colNo * outputChar)

    for colNo in range(numRepeats - 1, 0, -1):
        print(space * " ", colNo * outputChar)
        space += 1


def repeatHollow(numRepeats, outputChar):
    space = 0
    for colNo in range(1, numRepeats + 1):
        if colNo <= 2:
            print(colNo * outputChar)
        elif colNo > 1 < numRepeats:
            print(outputChar, colNo * "x", outputChar * 1)

    for colNo in range(numRepeats - 1, 0, -1):
        Space = 0
        i_space = numRepeats
        if colNo == 1:
            print(space * " ", outputChar)
        elif colNo > 1 < numRepeats:
            print(space * " ", outputChar, i_space * " ", outputChar * 1)
            space += 1
            numRepeats -= 1


def main():
    print("This program will output a parallelogram.")
    # numRepeats = int(input("How long do you want each side to be? "))
    numRepeats = 5
    # outputChar = input("Please enter the character you want it to be made of: ")
    outputChar = 'E'

    # whichOne= input("Enter 'A' for filled in Parallelogram, or 'B' for Hollowgram:")
    whichOne = 'B'
    if whichOne == 'A':
        print(repeatChar(numRepeats, outputChar))
    else:
        print(repeatHollow(numRepeats, outputChar))


main()