import shutil
from pathlib import Path
from typing import Any
import os

ERROR_MSG = "ERROR"
VALID_1ST_CHOICES = ['D',  # Directory
                     'R']  # Recursive
VALID_2ND_CHOICES = ['A',  # all files
                     'N',  # find exact matches file name (incl. extension)
                     'E',  # find files with extension (with or without dot)
                     'T',  # search text files for some string
                     '<',  # search for files smaller than some bytes
                     '>']  # # search for files larger than some bytes
VALID_3RD_CHOICES = ['F',  # print out first line of text files
                     'D',  # duplicate files
                     'T']  # touch files


def search() -> None:
    """
    Begins the search program.
    Allows user to search provided directory for files that meet certain criteria.
    :return: None.
    """
    # PROMPT 1 - directory or recursive search
    paths = prompt1()

    # PROMPT 2 - criteria to filter files
    paths = prompt2(paths)
    if paths is None:  # even if the file list is empty allow user to specify filter but stop after
        return None

    # PROMPT 3 - what to do to files
    if paths:
        prompt3(paths)




def prompt1() -> list[Path] or None:
    """
    Handles the first part of the program. Asks user for directory or recursive search.
    """
    prompt1_command, prompt1_path = get_valid_input(1)
    prompt1_path = Path(prompt1_path)  # str to Path
    if prompt1_command == 'D':  # directory
        paths = get_all_this_dir(prompt1_path)
    elif prompt1_command == 'R':  # recursive
        paths = get_all_recursive(prompt1_path)
    else:
        raise Exception("Invalid response for prompt 1")
    paths = process_paths(paths)
    if len(paths) == 0: return None  # stop the program if no interesting files are left
    print_paths(paths)
    return paths


def prompt2(paths: list[Path]) -> list[Path] or None:
    """
    Handles the second part of the program. Asks user how they want to filter the files
    """
    prompt2_command, prompt2_arg = get_valid_input(2)
    if paths is None:  # user's initial search yielded 0 files
        return None
    elif prompt2_command == 'A':  # all files
        pass
    elif prompt2_command == 'N':  # name
        paths = match_name(prompt2_arg, paths)
    elif prompt2_command == 'E':  # extension
        paths = match_extension(prompt2_arg, paths)
    elif prompt2_command == 'T':
        paths = match_string(prompt2_arg, paths)
    elif prompt2_command in ['<', '>']:
        paths = match_size(prompt2_arg, prompt2_command, paths)
    else:
        raise Exception("Invalid response for prompt 2")
    if len(paths) == 0: return None  # stop the program if no interesting files are left
    print_paths(paths)
    return paths


def prompt3(paths: list[Path]) -> None:
    prompt3_command = get_valid_input(3)[0]
    if prompt3_command == 'F':  # fist line
        print_first_lines(paths)
    elif prompt3_command == 'D':
        duplicate_files(paths)
    elif prompt3_command == 'T':
        touch_files(paths)
    else:
        raise Exception("Invalid response for prompt 3")



def sort_key_paths(path: Path) -> tuple[int, tuple[str]]:
    """
    Returns a string version of the path used in sorting.
    First the function finds out how many folders deep the path is. The more that later it appears in the sort.
    """
    num_folders = len(path.parts)  # the number of folder the final file/dir is in
    if path.is_file():
        num_folders -= 1  # last part is actually file not a dir
    folder_names = path.parts  # sort based off names

    return num_folders, folder_names


def sort_paths(paths: list[Path]) -> list[Path]:
    """
    Sort the list of paths into proper lexicographical order.
    Files come before directories. Files from main search directory come first.
    Does NOT sort in place!
    """
    return sorted(paths, key=sort_key_paths)


def is_valid_command(user_input: str, valid_l: list[str]) -> bool:
    """
    Determines ONLY if the search flag is valid. Ex ['D', 'R'] are valid for 1st question.
    """
    # uses short circuit eval if no command was entered no need to check if it was in valid_l
    return len(user_input) > 0 and user_input[0] in valid_l


def is_valid_format_two_args(user_input: str) -> bool:
    """
    Some commands require a flag and a path/name/string/extension/etc
    This ensures that the 2nd part is present and the whole string is formatted properly
    Does not validate anything beyond the format for name/path/etc.
    Returns True if the format is valid, else returns False
    """
    if len(user_input) >= 3:  # flag, space, at one char for name/path/etc
        # whitespace between the flag and name/path/etc, name/path/etc cannot be a whitespace
        if user_input[1] == ' ' and user_input[2] != ' ':
            return True

    return False


def is_valid_size(user_input: str) -> bool:
    """
    When searching for files of a certain size this functions ensures that size is actually a valid number.
    """
    try:
        user_input = int(user_input)
    except ValueError:  # something other than digits in the str
        return False

    return user_input >= 0  # file size cannot be negative


def get_valid_input(prompt_level: int) -> tuple[str, str]:
    """
    Asks (without prompt) the user for input that must be in valid_options
    or else user will be asked again repeatedly.
    Prints error message each time input is invalid.
    Some input requires flag plus a path/name/etc
    :param prompt_level: 1, 2, or 3. 1st is 'D', 'R', 2nd is 'A, 'N', etc, 3rd is 'F', 'D', 'T'
    """
    prompt_levels = {1: is_valid_response_1st_prompt,
                     2: is_valid_response_2nd_prompt,
                     3: is_valid_response_3rd_prompt}

    is_valid = prompt_levels.get(prompt_level)

    while True:
        user_input = input()

        if is_valid(user_input):
            return user_input[0], get_2nd_arg_user_input(user_input)  # the command letter and argument if it exists
        else:
            print(ERROR_MSG)


def is_valid_response_1st_prompt(user_input: str) -> bool:
    """
    First prompt that which ask for a directory to search either just
    that directory or a recursive search.
    This determines whether or not the response to the prompt is valid.
    Ex. 'D C:\St\Test\project1' or 'R C:\St\Test\project1'
    """
    return all([
        is_valid_command(user_input, VALID_1ST_CHOICES),
        is_valid_format_two_args(user_input),
        Path(get_2nd_arg_user_input(user_input)).is_dir()
    ])


def is_valid_response_2nd_prompt(user_input: str) -> bool:
    """
    Second prompt asks user what do with their list of Paths.
    This function ensures that what they want is valid.
    """
    # 'A'
    if user_input == 'A':
        return True

    # 'N', 'E', 'T'
    elif user_input[0] in ['N', 'E', 'T']:
        return is_valid_format_two_args(user_input)

    # '<', '>'
    elif user_input[0] in ['<', '>']:
        return all([
            is_valid_format_two_args(user_input),
            is_valid_size(get_2nd_arg_user_input(user_input))
        ])
    else:
        return False


def is_valid_response_3rd_prompt(user_input: str) -> bool:
    """
    Third prompt asks user if they want to print first line, duplicate, or touch the file.
    This function ensures that what they want is valid.
    """
    return all([
        is_valid_command(user_input, VALID_3RD_CHOICES),
        len(user_input) == 1
    ])


def get_2nd_arg_user_input(user_input: str) -> str:
    """
    Gets the second argument from the user input string.
    Ex. 'R C:\St\text.txt' -> 'C:\St\text.txt'
    Ex. 'T while True' -> 'while True'
    If string is less than 3 chars long then empty string is returned
    """
    return user_input[2:]


def is_valid_dir(path: Path) -> bool:
    """
    Return whether or not  the dir exists.
    """
    return path.is_dir()


def get_file_list(path: Path) -> list[Path]:
    """
    Finds all the files in specified directory.
    Assumes path is valid.
    Does not return sub-directories or their content
    """
    all_files = get_all_this_dir(path)  # includes files and sub dirs
    files = [p for p in all_files if p.is_file()]  # only include files
    return files


def get_all_this_dir(path: Path) -> list[Path]:
    """
    Gets all files and directories in the path.
    Does not get any sub files/directories.
    Returns an empty list if directory is locked.
    """
    try:
        return sort_paths(list(path.iterdir()))
    except PermissionError:
        return []


def get_all_recursive(path: Path) -> list[Path]:
    """
    Gets all sub/files, sub/directories from the specified path.
    Assumes path is valid.
    Result not sorted.
    """
    all_paths = get_all_this_dir(path)
    for p in all_paths:
        if p.is_dir():
            all_paths.extend(get_all_recursive(p))  # recursive exhaustive search

    all_paths = remove_duplicates(all_paths)  # if no sub-dirs in dir return just the files
    all_paths = sort_paths(all_paths)
    return all_paths


def remove_duplicates(my_list: list[Any]) -> list[Any]:
    """
    Removes duplicates from a list.
    Preserves order.
    """
    # convert to dict which can not have duplicate keys and then back to list
    return list(dict.fromkeys(my_list))


def print_paths(paths: list[Path]) -> None:
    """
    Prints each path on new line. Does NOT sort.
    """
    print(*paths, sep='\n')


def is_text_file(path: Path) -> bool:
    """
    Checks if the input file is a text file.
    Also returns false if a dir is specified.
    """
    try:
        if not path.is_file():  # to be a text file it first needs to be a file
            return False
    # when user filtered it so 0 files are in the list then this function will try and .is_file() and empty list
    except AttributeError:
        return False

    # If the file truly can't be opened.
    with open(path) as f:
        try:
            f.readlines()
        except (UnicodeDecodeError, FileNotFoundError, PermissionError):  # non-text file, DNE, locked
            return False

    return True


def touch_files(files: list[Path]) -> None:
    """
    Updates a list of files' modified time to current time
    """
    for file in files:
        file.touch()


def match_name(name: str, paths: list[Path]) -> list[Path]:
    """
    Searches a list of Path objects for those who's name EXACTLY matches (incl extension)
    the specified name
    """
    new_paths = []
    for path in paths:
        if path.is_file():
            if os.path.basename(path) == name:
                new_paths.append(path)

    return new_paths  # no need to sort again because the initial search did that.


def match_extension(ext: str, paths: list[Path]) -> list[Path]:
    """
    Creates a new list of Path objects that have the specified extension.
    Works with and without the period before file type.
    """
    # add the period before extension if it doesn't already exist
    if ext[0] != '.':
        ext = '.' + ext

    new_paths = []
    for path in paths:
        if path.is_file() and path.name.endswith(ext):  # Extensionless files and dirs will return '' suffix
            new_paths.append(path)

    return new_paths


def match_size(size_to_match: str, direction: str, paths: list[Path]) -> list[Path]:
    """
    Creates a new list of Path objects that are either smaller or larger than specified size.
    :param direction. '<' (look for smaller files), '<' (larger files)
    """
    size_to_match = int(size_to_match)
    new_paths = []

    for path in paths:
        if path.is_file():
            size = os.path.getsize(path)
            if direction == '<' and size < size_to_match:  # looking for smaller files and file is smaller
                new_paths.append(path)
            elif direction == '>' and size > size_to_match:  # looking for larger files and file is larger
                new_paths.append(path)

    return new_paths


def match_string(s: str, paths: list[Path]) -> list[Path]:
    """
    Searches text files (not just .txt) for an exact match of s.
    """
    new_paths = []

    for path in paths:
        if is_string_in_file(s, path):
            new_paths.append(path)

    return new_paths


def is_string_in_file(s: str, file: Path) -> bool:
    """
    Searches a text file for a exact match of a string.
    """
    if is_text_file(file):
        with open(file) as text_file:
            for line in text_file:
                if s in line:
                    return True

    return False


def print_first_line(path: Path) -> None:
    """
    Prints the first line if the path leads to a text file.
    Else it prints "NOT TEXT"
    """
    if is_text_file(path):
        with open(path) as text_file:
            first_line = text_file.readline()
            # or needed because this line relies on short circuit eval. Right side would IndexError if line == ''
            if first_line == '' or first_line[-1] != '\n':  # no new line at end, ensure there is when printed
                end = '\n'
            elif first_line[-1] == '\n':  # new line already at end, ensure no extra newline is printed
                end = ''
            else:
                raise Exception("line not valid.")
            print(first_line, end=end)  # read first line  #
    else:
        print("NOT TEXT")


def print_first_lines(paths: list[Path]) -> None:
    """
    Attempt to print the first line of several files. Prints "NOT TEXT" if the file is not a text file
    """
    for file in paths:
        print_first_line(file)


def duplicate_file(file: Path) -> None:
    """
    Duplicates a file. Appends ".dup" to the end of the name.
    Ignores files with locked permissions.
    """
    if file.is_file():
        new_file = file.with_name(file.name + ".dup")  # a safe way to append .dup to the file
        try:
            shutil.copy2(file, new_file)
        except PermissionError:  # skip files that can't be duplicated
            pass


def duplicate_files(files: list[Path]) -> None:
    """
    Duplicates files in a list.
    """
    for file in files:
        duplicate_file(file)


def remove_dirs(paths: list[Path]) -> list[Path]:
    """
    Removes directories from the paths list
    """
    new_paths = []
    for path in paths:
        if path.is_file():
            new_paths.append(path)

    return new_paths


def process_paths(paths: list[Path]) -> list[Path]:
    """
    removes duplicates, removes folders, and sorts paths
    """
    paths = remove_duplicates(paths)
    paths = remove_dirs(paths)
    paths = sort_paths(paths)
    return paths


if __name__ == "__main__":
    search()
